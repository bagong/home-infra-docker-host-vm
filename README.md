# Docker Host VM Provisioning Routines

Built for Ubuntu 20.04 LTS VM.


## Usage

1. Install ansible and dependencies on local machine
```bash
sudo apt install python3-pip pwgen sshpass
sudo pip3 install ansible
ansible-playbook --version  # should output version > 2.10
```

2. Adjust target server hostname/IP in `inventory.yaml`

3. Run `ansible-playbook`
```bash
# to check
ansible-playbook -i production --check site.yaml

# to deploy
ansible-playbook -i production site.yaml
```

## Notes

- Following directory structure from https://docs.ansible.com/ansible/2.3/playbooks_best_practices.html#directory-layout


### Testing
Tests using [Molecule](https://molecule.readthedocs.io). Installation follows:

```bash
sudo pip3 install molecule
sudo pip3 install molecule-docker
```

Documentation:
- https://molecule.readthedocs.io/en/latest/getting-started.html
- https://www.jeffgeerling.com/blog/2018/testing-your-ansible-roles-molecule


## TODO

- Testing
- User management
    - add my current local user to remote, if not yet present
    - copy ssh pubkey
- SSH basic hardening
    - disable root login
    - disable password login
    - enable pubkey
    - check gh:do-community/ansible-playbooks
    - check gh:dev-sec/ansible-ssh-hardening
- Basic setup for docker host
    - `nfs-common` apt package
    - NFS mount target in `/etc/fstab`
        - `/mnt/docker_volumes` general docker volumes for persistent config etc.
        - `/mnt/docker_nextcloud` data and contents for nextcloud
- Docker installation and basic config
- Services in docker containers
    - Reverse Proxy --> docker
    - Docker container management --> portainer
    - nextcloud
    - heimdall
    - pihole
        - disable `systemd-resolved`
        - set dns resolver to localhost
        - make sure IP address is consistent, either static IP from DHCP or
          local
    - photoprism
